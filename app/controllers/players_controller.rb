class PlayersController < ApplicationController
  http_basic_authenticate_with name: "admin", password: "brian",
                                    only: [:show, :destroy]
  
  def index
    @day = day_to_display
    redirect_to action: 'schedule' if !day_to_display 
    @player = Player.new
    @player_count = Player.all.count
  end
  
  def show
    @player = Player.all
  end
  
  def create
    @player = Player.new
    all_ips = Player.uniq.pluck(:ip)    # check for unique IP
    all_ips.each do |ip|
      if ip == request.ip
        flash[:danger] = "You have already signed up!"
        # render 'index'
        redirect_to action: 'index'
        return
      end
    end
    
    params[:player][:ip] = request.ip
    @player = Player.new(player_params)
   
    if @player.save
      flash[:info] = "You are signed up!"
      redirect_to action: 'index'
    else
      flash[:danger] = "Name (2-15 letters) and Phone Number (min 10 digits) Required"
      redirect_to action: 'index'
    end
  end
  
  def schedule
  end
  
  def superadmin
  end
  
  def delete_all
    Player.delete_all
    redirect_to action: 'index'
  end
  
  

  private
  def player_params
    params.require(:player).permit(:name, :phone_number, :ip)
  end

  def day_to_display
    
    day = Time.now.wday; hour = Time.now.hour - 5
    day = 6; hour = 9 # manual testing
    
    if (day == 2 && hour >= 16) || (day == 3 && hour < 10)
      "Wednesday"
      elsif (day == 4 && hour >= 16) || (day == 5 && hour < 10)
      "Friday"
      elsif (day == 5 && hour >= 16) || (day == 6 && hour < 10)
      "Saturday"
      else
        false
    end
  end
end
