class Player < ActiveRecord::Base
  
  validates :name, presence: true, length: { minimum: 2, maximum: 15 }
  validates :phone_number, presence: true, length: { minimum: 10, maximum: 14 }  #, phony_plausible: true
  # phony_normalize :phone_number, default_country_code: 'US'
  
end
