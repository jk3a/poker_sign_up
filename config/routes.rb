Rails.application.routes.draw do
  root 'players#index'
  post '/', to: 'players#create'
  get 'players', to: 'players#show'
  get 'schedule', to: 'players#schedule'
  get 'superadmin', to: 'players#superadmin'
  delete 'delete_all', to: 'players#delete_all'
end
